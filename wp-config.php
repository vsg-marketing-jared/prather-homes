<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'vsg360_wp3');

/** MySQL database username */
define('DB_USER', 'vsg360_wp3');

/** MySQL database password */
define('DB_PASSWORD', 'Z.noVO1Z7vYXT1O2aVa02');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '1h5YUuo2HKBbGWjgMBUW8Sij4k6Z86hgRdr7XBgRuq9cPSrzbnFsH6T9SsbqcuuT');
define('SECURE_AUTH_KEY',  'x5vxnkcp5OfkQkNQRp3KFJX75CpdbqyB4eNsi4X7M9prybrWA94n1XukJM5ssi9k');
define('LOGGED_IN_KEY',    'aLhQxCs07H6QLpHqlAjqFJExj0Qnt8TSe9nQ53OTwiV9Lf6DWBXLijejaAXSAUjq');
define('NONCE_KEY',        'E9JIQADNpl89Ock9esbvmifrVDBu90SI57qFLKZVWROnApHgVc72MXiI2bvsD0At');
define('AUTH_SALT',        'GeMgxf4sI5JSz672Bu6OZ5jk7OmNOQAISLwQ5kWMM3IKjRXUqqv4rCJ8XZm2UnxN');
define('SECURE_AUTH_SALT', 'oMiQ46CA3tOFVu55psnai8JV8VhRnvOdxgyC3mCEbrnttGD2GXLE0O5YFYm2okI0');
define('LOGGED_IN_SALT',   'lIRLwoguCqESyom1IjsXaDHSsrdYxJaMgViiWVSlZZaqfTB1kPlDjhLRtFnCPRhO');
define('NONCE_SALT',       'eDc3JQcbyV01lGJZH9tO9DqgFBJFdqzar62d8QGLi8E9DJuV5o1eQ7gWIspM8UC9');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
