<?php
/**
 * Plugin Name: Prather Homes
 * Description: Custom functionality and styles for Prather Homes.
 * Version: 1.0.0
 * Author: Jared Eddy
 * Author URI: http://jarededdy.io
 */

 
class PratherHomesCustomization {

    public function __construct() {

        // Add Javascript and CSS for front-end display
        add_action('wp_enqueue_scripts', array($this,'enqueue'));

        // add_action('init', [$this, 'register']);
    }

    // This is an example of enqueuing a JavaScript file and a CSS file for use on the front end display
    public function enqueue() {
    	wp_enqueue_script('ph-custom-scripts', plugins_url('js/custom-scripts.js', __FILE__), ['jquery', 'elementor-pro-frontend'], false, true);
        wp_enqueue_style('ph-custom-styles', plugins_url('css/style.min.css', __FILE__), null, '1.0');
    }

    // Our custom post type function
    // public function register() {
    
    //     register_post_type( 'VSG Portfolio',

    //     array(
    //         'labels' => array(
    //             'name' => __( 'VSG Portfolio' ),
    //             'singular_name' => __( 'Work' )
    //         ),
    //         'public' => true,
    //         'has_archive' => true,
    //         'rewrite' => array('slug' => 'vsg-portfolio'),
    //     )
    // );
    // }

}

// Create an instance of our class to kick off the whole thing
$vsgMarketing = new PratherHomesCustomization();
